(ns patient-front.views-handlers
  (:require-macros [cljs.core.async.macros :as async-m])
  (:require
   [re-frame.core :refer [subscribe dispatch]]
   [patient-front.validate :as v]
   [cljs.core.async :as async])
  (:import [goog.async Debouncer]))



(def delay-time 500)

(defn input-val [e]
  (-> e .-target .-value))

(defn handle-str [val keyw from to db-keyw]
  (dispatch [:val-len-from-to keyw val from to])
  (dispatch [:val-only-eng keyw val])
  (dispatch [db-keyw {keyw val}]))

(defn handle-firstname [val db-keyw]
  (handle-str val :firstname 3 64 db-keyw))

(defn handle-secondname [val db-keyw]
  (handle-str val :secondname 3 64 db-keyw))

(defn handle-sex [val db-keyw]
  (dispatch [:val-in :sex val ["MALE" "FEMALE"]])
  (dispatch [db-keyw {:sex val}]))

(defn handle-address [val db-keyw]
  (dispatch [:val-len-from-to :address val 0 200])
  (dispatch [db-keyw {:address val}]))

(defn handle-birthdate [val db-keyw]
  (dispatch [:val-date :birthdate val])
  (dispatch [db-keyw {:birthdate val}]))

(defn handle-polys [val db-keyw]
  (dispatch [:val-between :polys (js/parseInt val) 1000000000000000 9999999999999999])
  (dispatch [db-keyw {:polys (js/parseInt val)}]))

(defn debounce [f interval]
  (let [dbnc (Debouncer. f interval)]
    (fn [& args] (.apply (.-fire dbnc) dbnc (to-array args)))))

(defn deb [f]
  (debounce (fn [e key] (f (input-val e) key)) delay-time))

(def firstname-on-change
  (deb handle-firstname))

(def secondname-on-change
  (deb handle-secondname))

(def sex-on-change
  (deb handle-sex))

(def address-on-change
  (deb handle-address))

(def birthdate-on-change
  (deb handle-birthdate))

(def polys-on-change
  (deb handle-polys))
