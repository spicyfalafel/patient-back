(ns patient-front.validate
  (:require [cljs-time.core :as t]
            [cljs-time.format :as f]))

(defn len-in [s from to]
  (if (<= from (count s) to)
      true
      false))
(defn only-eng [s]
  (if (re-matches #"^[A-Za-z\-]+$" s)
      true
      false))

(defn all-digits [s]
  (if (re-matches #"^\d+$" s)
      true
      false))

(defn val-date [date-str]
  (try
    (let [d (f/parse (f/formatter "yyyy-MM-dd") date-str)]
      (if d
        (t/before? d (t/now))
        false))
    (catch js/Error e
      false)))

(defn valid-input? [mp]
  (every? (fn [[_ val]] (true? val)) mp))

(defn valid-inputs? [input-mp]
  (every? valid-input? (vals input-mp)))

