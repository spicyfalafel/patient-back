(ns patient-front.subs
  (:require
   [re-frame.core :as re-frame :refer [reg-sub]]))


(reg-sub
 :active-page
 (fn [db _]
   (:active-page db)))

(reg-sub
 :patients
 (fn [db _]
   (:patients db)))

(reg-sub
 :edit-patient
 (fn [db _]
   (:edit-patient db)))

(reg-sub
 :errors
 (fn [db _]
   (:errors db)))

(reg-sub
 :add-patient
 (fn [db _]
   (:add-patient db)))

(reg-sub
 :search
 (fn [db _]
   (:search-patients db)))
