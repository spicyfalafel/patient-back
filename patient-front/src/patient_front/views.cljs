(ns patient-front.views
  (:require
   [re-frame.core :refer [subscribe dispatch]]
   [patient-front.routes :as routes :refer [url-for]]
   #_:clj-kondo/ignore
   [patient-front.subs :as subs]
   [patient-front.validate :as v]
   [patient-front.views-handlers :as h]))

(defn input-val [e]
  (-> e .-target .-value))

(defn edit-patient-row [p]
  (let [errors @(subscribe [:errors])]
    [:tr {:key (:id p)}
     [:th {:scope :row}
      [:div
       (:id p)]]
     [:td
      [:input.form-control {:placeholder (:firstname p)
                            :on-change #(h/firstname-on-change % :edit-patient)}]
      [:label.form-label.small-lbl.ms-3  "3-64 chars"]]
     [:td
      [:input.form-control {:placeholder (:secondname p)
                            :on-change #(h/secondname-on-change % :edit-patient)}]
      [:label.form-label.small-lbl.ms-3  "3-64 chars"]]
     [:td
      [:div
       [:input.form-control {:list :sex-list :id :sex-input :placeholder (:sex p)
                             :on-change #(h/sex-on-change % :edit-patient)}]
       [:datalist {:id :sex-list}
        [:option {:value "MALE"}]
        [:option {:value "FEMALE"}]]]]
     [:td
      [:input.form-control {:id :birthdate :placeholder (:birthdate p)
                            :on-change #(h/birthdate-on-change % :edit-patient)}]
      [:label.form-label.small-lbl.ms-3  "YYYY-MM-DD"]]
     [:td
      [:input.form-control {:placeholder (:address p)
                            :on-change #(h/address-on-change % :edit-patient)}]]
     [:td
      [:input.form-control {:placeholder (:polys p)
                            :on-change #(h/polys-on-change % :edit-patient)}]

      [:label.form-label.small-lbl.ms-3  "16 digits"]]
     [:td
      [:div
       [:button.btn.btn-primary.btn-block.fixed-sz
        {:on-click (fn [_] (dispatch [:edit-patient-done]))
         :disabled (not (v/valid-inputs? errors))}
        "Done"]]]]))

(defn patient-row [p]
  [:tr {:key (:id p)}
   [:th {:scope :row} (:id p)]
   [:td (:firstname p)]
   [:td (:secondname p)]
   [:td (:sex p)]
   [:td (:birthdate p)]
   [:td (:address p)]
   [:td (:polys p)]
   [:td
    [:button.btn.btn-danger.fixed-sz
     {:on-click #(dispatch [:delete-patient (:id p)])} "Delete"]]
   [:td
    [:button.btn.btn-primary.fixed-sz {:on-click #(dispatch [:set-edit-patient p])}
                                      "Edit"]]])

(defn home []
  (let [patients @(subscribe [:patients])
        edit @(subscribe [:edit-patient])
        search @(subscribe [:search])]
    [:div
     [:table.table.table-hover
      [:thead
       [:tr
        [:th.cursored {:scope :col
                       :on-click #(dispatch [:patients-sort-by :id])}
         "Id"]
        [:th.cursored {:scope :col
                       :on-click #(dispatch [:patients-sort-by :firstname])}
         "First name"]
        [:th.cursored {:scope :col
                       :on-click #(dispatch [:patients-sort-by :secondname])} "Second name"]
        [:th.cursored {:scope :col
                       :on-click #(dispatch [:patients-sort-by :sex])} "Sex"]
        [:th.cursored {:scope :col
                       :on-click #(dispatch [:patients-sort-by :birthdate])} "Date of birth"]
        [:th.cursored {:scope :col
                       :on-click #(dispatch [:patients-sort-by :address])} "Address"]
        [:th.cursored {:scope :col
                       :on-click #(dispatch [:patients-sort-by :polys])} "Polys"]]]
      [:tbody
       (doall
        (let [pts (if search search patients)]
          (for [[_ p] pts]
            (if (= (:id edit) (:id p))
              (edit-patient-row p)
              (patient-row p)))))]]]))

(defn add-patient-form []
  [:div.row.align-items-center.justify-content-center {:id :login-div}
   [:form.text-center
    [:div.form-outline
     [:input.form-control {:id :firstname
                           :type :text
                           :on-change #(h/firstname-on-change % :add-patient)}]
     [:label.form-label {:for :firstname} "First name"]]
    [:div.form-outline
     [:input.form-control {:id :secondname
                           :type :text
                           :on-change #(h/secondname-on-change % :add-patient)}]
     [:label.form-label {:for :secondname} "Second name"]]
    [:div.form-outline
     [:input.form-control {:id :sex
                           :type :text
                           :on-change #(h/sex-on-change % :add-patient)}]
     [:label.form-label {:for :sex} "Sex"]]
    [:div.form-outline
     [:input.form-control {:id :birthdate
                           :type :text
                           :on-change #(h/birthdate-on-change % :add-patient)}]
     [:label.form-label {:for :birthdate} "Date of birth"]]
    [:div.form-outline
     [:input.form-control {:id :address
                           :type :text
                           :on-change #(h/address-on-change % :add-patient)}]
     [:label.form-label {:for :address} "Address"]]
    [:div.form-outline
     [:input.form-control {:id :polys
                           :type :text
                           :placeholder "1234 5678 1234 5678"
                           :on-change #(h/polys-on-change % :add-patient)}]
     [:label.form-label {:for :polys} "Polys id"]]]])

(defn errors-view [errors]
  (for [[k v] errors]
    [:div {:key k} (name k) ": "
          (if (v/valid-input? v)
            "OK!"
             "WRONG!")
     "\n"]))

(defn add-patient-modal []
  (let [errors @(subscribe [:errors])]
    [:div
     [:a.nav-link {:type "button" :data-bs-toggle "modal", :data-bs-target "#addModal"}
                  "Add"]
     [:div.modal {:id "addModal" :aria-labelledby "exampleModalLabel", :aria-hidden "true"}
      [:div.modal-dialog
       [:div.modal-content
        [:div.modal-header
         [:h5.modal-title "Add patient"]
         [:button.btn-close {:type "button" :data-bs-dismiss "modal", :aria-label "Close"}]]
        [:div.modal-body
         (add-patient-form)
         [:div (errors-view errors)]]
        [:div.modal-footer
         [:button.btn.btn-secondary {:type "button", :data-bs-dismiss "modal"
                                     :on-click #(dispatch [:reset-errors])} "Close"]
         [:button.btn.btn-primary {:type "button"
                                   :on-click #(dispatch [:add-patient-done])
                                   :disabled (not (v/valid-inputs? errors))}
          "Save changes"]]]]]]))

(defn search-view []
  [:form.search
   [:input.form-control ;.query
    {:placeholder "Looking for?"
     :on-change #(dispatch [:search-patients (input-val %)])}]])

(defn header []
  [:div
   [:nav.navbar.navbar-light.navbar-expand-lg.bg-light
     [:a.navbar-brand {:href (url-for :home)} "Homework"]
     [:div.collapse.navbar-collapse
      [:ul.navbar-nav.mr-auto
       [:li.nav-item.active
         (add-patient-modal)]]
      [:ul.navbar-nav.mr-auto
       [:li.nav-item.active
         (search-view)]]]]])

(defn pages
  [page-name]
  (case page-name
    :home [home]
    [home]))

(defn main-panel []
  (let [active-page @(subscribe [:active-page])]
    [:div
     [header]
     [pages active-page]]))
