(ns patient-front.events
  (:require
   [re-frame.core :as re-frame :refer [dispatch reg-event-fx
                                       reg-event-db reg-fx]]
   [patient-front.db :as db :refer [default-db]]
   [ajax.core :refer [json-request-format json-response-format text-response-format]]
   [day8.re-frame.http-fx]
   [day8.re-frame.tracing :refer-macros [fn-traced]]
   [clojure.string :as str]
   [patient-front.routes :as routes]
   [patient-front.validate :as v])
  (:require-macros [adzerk.env :as env]))

;; -- helpers ------------------------------------------------------------------
;;

(env/def BACKEND :required)


(def host
  (do (println BACKEND)
    (if BACKEND
      BACKEND
      "")))

(def api-url (str host "/api"))
  ;(str host ":" port "/api"))


(defn endpoint
  "Concat any params to api-url separated by /"
  [& params]
  (str/join "/" (cons api-url params)))


;; -- Events -------------------------------------------------------------------
;;

(reg-event-fx
 :initialize-db
 (fn [_ _]
   {:db default-db}))

(reg-event-fx                                            ;; usage: (dispatch [:set-active-page {:page :home})
 :set-active-page                                        ;; triggered when the user clicks on a link that redirects to another page
 (fn [{:keys [db]} [_ {:keys [page]}]] ;; destructure 2nd parameter to obtain keys
   (let [set-page (assoc db :active-page page)]
     (case page
       :home {:db         set-page
              :dispatch [:get-patients]}
       ; (:login :register :settings) {:db set-page} ;; `case` can group multiple clauses that do the same thing.
       {:db set-page}))))


; ----------------------------- Get --------------------------------------------
(reg-event-fx
 :get-patients
 (fn [_ _]
     {:http-xhrio {:method          :get
                   :uri             (endpoint "patient")
                   :format          (json-request-format)
                   :response-format (json-response-format {:keywords? true})
                   :on-success      [:get-patients-success]
                   :on-failure      [:api-request-error {:request-type :get-patients}]}}))

(defn set-ids [pats]
  (reduce merge (map #(hash-map (:id %) %) pats)))

(reg-event-fx
 :get-patients-success
 (fn [{:keys [db]} [_ patients]]
   {:db (assoc-in db [:patients]
                  (set-ids patients))}))

;; --- Validate ----------------------------------------------------------------
(reg-event-fx
 :val-len-from-to
 (fn [{:keys [db]} [_ key value from to]]
   {:db (assoc-in db [:errors key :len] (v/len-in value from to))}))

(reg-event-fx
 :val-only-eng
 (fn [{:keys [db]} [_ key value]]
   {:db (assoc-in db [:errors key :only-eng]
                  (v/only-eng value))}))

(reg-event-fx
 :val-in
 (fn [{:keys [db]} [_ key val good-vals]]
   {:db (assoc-in db [:errors key :in]
                  (if (some #{val} good-vals)
                    true
                    false))}))
(reg-event-fx
 :val-between
 (fn [{:keys [db]} [_ key val from to]]
   {:db (assoc-in db [:errors key :between]
                   (if (<= from val to)
                    true
                    false))}))

(reg-event-fx
 :val-date
 (fn [{:keys [db]} [_ key date-str]]
   {:db (assoc-in db [:errors key :date]
                  (v/val-date date-str))}))

;----------------------------- DELETE ------------------------------------------
(reg-event-fx
 :delete-patient
 (fn [_ [_ pat-id]]
   {:http-xhrio {:method          :delete
                 :uri             (endpoint "patient" (str pat-id))
                 :format          (json-request-format)
                 :response-format (json-response-format {:keywords? true})
                 :on-success      [:delete-patient-front-db pat-id]
                 :on-failure      [:api-request-error {:request-type :delete-patient}]}}))

(reg-event-fx
 :delete-patient-front-db
 (fn [{:keys [db]} [_ patient-id]]
   {:db
     (update-in db [:patients] dissoc patient-id)}))

;; --- Edit --------------------------------------------------------------------
(reg-event-fx
 :set-edit-patient
 (fn [{:keys [db]} [_ pat]]
    {:db (assoc db :edit-patient pat)}))

(reg-event-fx
 :edit-patient
 (fn [{:keys [db]} [_ pat]]
   {:db (update-in db [:edit-patient] merge pat)}))


(reg-event-fx
 :edit-patient-done
 (fn [{:keys [db]} _]
   {:http-xhrio {:method          :patch
                 :uri             (endpoint "patient" (-> db :edit-patient :id))
                 :params          (:edit-patient db)
                 :format          (json-request-format)
                 :response-format (json-response-format)
                 :on-success      [:edit-patient-success (:edit-patient db)]
                 :on-failure      [:api-request-error {:request-type :edit-patient-done}]}}))

(reg-event-fx
 :edit-patient-reset
 (fn [{:keys [db]} _]
    {:db (assoc db :edit-patient {:id -1})}))


;; --- Update ------------------------------------------------------------------
(reg-event-fx
 :patient-update
 (fn [{:keys [db]} [_ patient]]
   {:db (assoc-in db [:patients (:id patient)] patient)}))

;;--- Adding -------------------------------------------------------------------
;; --- Search ------------------------------------------------------------------
(reg-event-fx
 :edit-patient-success
 (fn [_ [_ patient]]
   {:fx [[:dispatch-n (list [:patient-update patient]
                            [:edit-patient-reset]
                            [:reset-errors])]]}))

(reg-event-fx
 :add-patient
 (fn [{:keys [db]} [_ patient]]
   {:db (update-in db [:add-patient] merge patient)}))

(reg-event-fx
 :add-patient-done
 (fn [{:keys [db]} _]
   {:http-xhrio {:method          :post
                 :uri             (endpoint "patient" (-> db :add-patient :id))
                 :params          (:add-patient db)
                 :format          (json-request-format)
                 :response-format (json-response-format {:keywords? true})
                 :on-success      [:add-patient-success]
                 :on-failure      [:api-request-error {:request-type :add-patient-done}]}}))

(reg-event-fx
 :add-patient-success
 (fn [{:keys [db]} [_ patient]]
   {:fx [[:dispatch [:reset-errors]]]
    :db (-> db (assoc :add-patient {})
               (assoc-in [:patients (:id patient)] patient))}))


;; --- Sorting -----------------------------------------------------------------
(reg-event-fx
 :patients-sort-by
 (fn [{:keys [db]} [_ key]]
   (let [sort (-> db :sort-by key)
         compar1 #(compare %2 %1)
         compar2 #(compare %1 %2)
         cmp (if sort compar1 compar2)
         sort-fn #(sort-by (fn [[_ v]] (key v))
                           cmp %)]
     {:db (-> db
              (update :patients sort-fn)
              (update-in [:sort-by key] not))})))

;; --- Search ------------------------------------------------------------------
(defn get-rid-of-keys [mp]
  (map (fn [[_ v]] v) mp))

(defn search-map [coll X]
  (filter (fn [mp]
            (some #{true} (for [[_ v] mp]
                            (when (= v X) true))))
          coll))

(defn search-patients [patients X]
  (set-ids (search-map (get-rid-of-keys patients) X)))

(reg-event-fx
 :search-patients
 (fn [{:keys [db]} [_ s]]
   {:db (assoc db :search-patients (search-patients (:patients db) s))}))

(reg-event-fx
 :search-patients-remove
 (fn [{:keys [db]} _]
   {:db (dissoc db :search-patients)}))


;; -- Request Handlers ---------------------------------------------------------
;;

(reg-event-fx
 :reset-errors
 (fn [{:keys [db]} _]
   {:db (dissoc db :errors)}))

(reg-event-db                                         ;; usage (dispatch [:api-request-error {:request-type <error-to-log-as>}])
 :api-request-error                                   ;; triggered when we get request-error from the server
 (fn [db [_ {:keys [request-type]} response]] ;; `response` is implicitly conj'ed as the last entry by :http-xhrio event.
   (-> db                                             ;; when we complete a request we need to clean so that our ui is nice and tidy
       (assoc-in [:api-errors request-type] (get-in response [:response :errors])))))
