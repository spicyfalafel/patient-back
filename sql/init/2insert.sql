insert into patient (id, firstname, secondname, sex, birthdate, address, polys) values
  (1, 'Ivan', 'Ivanov', 'MALE', '01-01-2001', 'Address1', 1111111111111111),
  (2, 'Grigory', 'Grigoriev','MALE', '02-02-2002', 'Address2', 1111111111111112),
  (3, 'Vyacheslav', 'Vyachelavov', 'MALE', '03-03-2003', 'Address3', 1111111111111113),
  (4, 'Andrey', 'Andreev', 'MALE', '04-04-2004', 'Address4', 1111111111111114),
  (5, 'Ann', 'Ivanova', 'FEMALE', '05-05-2005', 'Address5', 1111111111111115);
ALTER SEQUENCE patient_id_seq RESTART WITH 6;