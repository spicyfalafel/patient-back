create table patient
(
    id         serial primary key,
    firstname   varchar(64) not null,
    secondname varchar(64) not null,
    sex text not null, -- можно вынести в другую табл 
    birthdate date,
    address text,
    -- 16 цифр
    polys bigint unique check (polys <= 9999999999999999 and polys >= 1000000000000000)
);
