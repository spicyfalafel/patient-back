(ns patient-back.database
  (:refer-clojure :exclude [group-by update partition-by filter for])
  (:require
   [clojure.java.jdbc :as jdbc]
   [clojure.edn :as edn]
   [clojure.java.io :as io]
   [honey.sql :as sql]
   [environ.core :refer [env]])
  (:import [java.sql Timestamp]
           [java.sql Date]
           [java.time.format DateTimeFormatter]
           [java.time LocalDate]
           [java.time Instant]
           [java.io FileWriter]))

;;----------------------CONFIG--------------------------------------------------
(defn load-edn
  "Load edn from an io/reader source (filename or io/resource)."
  [source]
  (try
    (with-open [r (io/reader source)]
      (println "Reading " source)
      (edn/read (java.io.PushbackReader. r)))
    (catch java.io.IOException e
      (printf "Couldn't open '%s': %s\n" source (.getMessage e)))
    (catch RuntimeException e
      (printf "Error parsing edn file '%s': %s\n" source (.getMessage e)))))

(def default-config
  {:dbtype "postgresql"
   :dbname "postgres"
   :host "localhost"
   :user "postgres"
   :port 5432
   :password "root"})

(def default-config-file "database-config.edn")

(def back-db
  (let [env-config (env :database-config)
        _ (println "env config: " env-config)]
    (if env-config
      (load-edn env-config)
      (if-let [file-config (load-edn default-config-file)]
          file-config
          default-config))))

;;---------------------------date insertion fix---------------------------------
(extend-protocol jdbc/IResultSetReadColumn
  java.sql.Timestamp
  (result-set-read-column [v _ _]
    (.toInstant v))
  java.sql.Date
  (result-set-read-column [v _ _]
    (.toLocalDate v)))

(extend-protocol jdbc/ISQLValue
  java.time.Instant
  (sql-value [v]
    (Timestamp/from v))
  java.time.LocalDate
  (sql-value [v]
    (Date/valueOf v)))
;;------------------------------------------------------------------------------

(defmethod print-method java.time.Instant
  [inst out]
  (.write out (str "#time/inst \"" (.toString inst) "\"")))

(defmethod print-dup java.time.Instant
  [inst out]
  (.write out (str "#time/inst \"" (.toString inst) "\"")))

(defmethod print-method LocalDate
  [^LocalDate date ^FileWriter out]
  (.write out (str "#time/ld \"" (.toString date) "\"")))

(defmethod print-dup LocalDate
  [^LocalDate date ^FileWriter out]
  (.write out (str "#time/ld \"" (.toString date) "\"")))

(defn parse-date [string]
  (LocalDate/parse string))

(defn parse-time [string]
  (and string (-> (.parse (DateTimeFormatter/ISO_INSTANT) string)
                  Instant/from)))

;;----------------PATIENTS------------------------------------------------------

(defn get-patients [db]
  (jdbc/query db (sql/format {:select [:*]
                              :from [:patient]})))

(defn get-patient [db id]
  (first (jdbc/query db (sql/format {:select [:*]
                                     :from [:patient]
                                     :where [:= :id id]}))))

(defn delete-patient! [db id]
  (when (= '(1)
            (jdbc/delete! db :patient ["id = ?" id]))
    true))

(defn replace-birthdate-str [patient]
  (try
    (if-let [birthdate-str (:birthdate patient)]
      (assoc patient :birthdate (parse-date birthdate-str))
      patient)
    (catch Exception e patient)))

(defn update-patient! [db patient]
  ;; если в параметре есть birthdate, значит надо его поменять на объект даты
  ;; если нет, значит менять не надо
  (= '(1) (jdbc/update! db :patient (replace-birthdate-str patient)
                     ["id = ?" (:id patient)])))

(defn insert-patient!
  ([db patient]
   (let [pat (replace-birthdate-str patient)]
     (first (jdbc/insert! db :patient pat)))))

