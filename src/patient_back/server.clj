(ns patient-back.server
  (:require
   [org.httpkit.server :as http-kit]
   [compojure.route :as cjr]
   [compojure.core :as c]
   [cheshire.generate :refer [JSONable]]
   [ring.middleware.cors :refer [wrap-cors]]
   [ring.middleware.params :refer [wrap-params]]
   [ring.middleware.keyword-params :refer [wrap-keyword-params]]
   [ring.middleware.json :refer [wrap-json-response wrap-json-body]]
   [ring.middleware.session :refer [wrap-session]]
   [ring.middleware.session.cookie]
   [patient-back.database :as db]
   [patient-back.validate :as v])
  (:gen-class))

(defmacro if-let*
  ([bindings then]
   `(if-let* ~bindings ~then nil))
  ([bindings then else]
   (if (seq bindings)
     `(if-let [~(first bindings) ~(second bindings)]
        (if-let* ~(drop 2 bindings) ~then ~else)
        ~else)
     then)))

;; LocalDate json encoding
(extend-protocol JSONable
  java.time.LocalDate
  (to-json [dt gen]
    (cheshire.generate/write-string gen (str dt))))


(defn get-patients [db]
  (let [patients (db/get-patients db)]
    {:status 200
     :body patients}))

(defn get-patient [id db]
  (if-let [pat (db/get-patient db (Integer/parseInt id))]
    {:status 200
     :body pat}
    {:status 404
     :body {:error "No such id"}}))

(defn add-patient [request db]
  (let [body (-> request :body v/patient-select)]
    (if-not (v/valid-patient? body)
      {:status 400
       :body {:error "Wrong data"}}
      {:status 200
       :body (db/insert-patient! db body)})))

(defn update-patient [id request db]
  (let [pat-upd-id (-> request :body (assoc :id (Integer/parseInt id)))]
    (if (v/valid-patient? pat-upd-id)
      (do
        (if (db/update-patient! db pat-upd-id)
            {:status 200
             :body pat-upd-id}
            {:status 400
             :body {:error "Wrong data"}}))
      {:status 400
       :body {:error "Wrong data"}})))

(defn delete-patient [id db]
  (if-let [a (db/delete-patient! db (Integer/parseInt id))]
     {:status 200
      :headers {"Content-Type" "text/plain"}
      :body {}}
    {:status 404
     :body {:error "No such id"}}))

(defn dispatch [db req]
  (let [r (c/defroutes routes
            (c/context "/api" []
              (c/context "/patient" []
                (c/GET "/" _(get-patients db))
                (c/POST "/" request (add-patient request db))
                (c/context "/:id" [id]
                  (c/GET "/" _ (get-patient id db))
                  (c/PATCH "/" request (update-patient id request db))
                  (c/DELETE "/" _ (delete-patient id db)))))
            (cjr/not-found "<h1>Page not found!!!</h1>"))]
    (r req)))

(defn handler [db req]
  (let [h (-> #(dispatch db %)
              wrap-json-response
              (wrap-json-body {:keywords? true :bigdecimals? true})
              wrap-params
              wrap-keyword-params
              (wrap-cors :access-control-allow-origin [#"http:/.*"] ;; CORS
                         :access-control-allow-methods [:get :post :delete :patch]
                         :access-control-allow-headers
                         ["Origin" "X-Requested-With" "Content-Type" "Accept"]))]
    (h req)))

(defn -main [& args]
  (let [args-map (apply array-map args)
        port-str (or (get args-map "-p")
                     (get args-map "--port")
                     "8080")]
    (println "Starting web server on port" port-str)
    (http-kit/run-server (fn [req] (handler db/back-db req)) {:host "0.0.0.0" :port 8080})))

(comment
  (def server (-main "--port" "8080"))

  (server)

  42)


