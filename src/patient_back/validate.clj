(ns patient-back.validate
  (:require [java-time :as t]))

(defn len-in? [s from to]
  (if (<= from (count s) to)
    true
    false))

(defn only-eng? [s]
  (if (re-matches #"^[A-Za-z\-]+$" s)
    true
    false))

(defn all-digits? [s]
  (if (re-matches #"^\d+$" s)
    true
    false))

(defn val-date? [date-str]
  (try
    (let [d (t/local-date "yyyy-MM-dd" date-str)]
      (if d
        (t/before? d (t/plus (t/local-date) (t/days 1)))
        false))
    (catch Exception _ false)))

(defn patient-select [mp]
  (select-keys mp [:firstname :secondname :birthdate :sex :address :polys]))

(defn valid-patient? [pat-mp]
  (and (or (nil? (:id pat-mp)) (<= 1 (:id pat-mp Integer/MAX_VALUE)))
       (len-in? (:firstname pat-mp) 3 64)
       (only-eng? (:firstname pat-mp))
       (len-in? (:secondname pat-mp) 3 64)
       (only-eng? (:secondname pat-mp))
       (or (nil? (:birthdate pat-mp)) (val-date? (:birthdate pat-mp)))
       (some #{(:sex pat-mp)} ["MALE" "FEMALE"])
       (or (nil? (:polys pat-mp)) (<= 1000000000000000 (:polys pat-mp) 9999999999999999))
       (or (nil? (:address pat-mp)) (len-in? (:address pat-mp) 0 1000))))

(defn contains-many? [m & ks]
  (every? #(contains? m %) ks))
