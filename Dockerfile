FROM openjdk:11-jre

ADD target/patient-back-0.1.0-SNAPSHOT-standalone.jar /backend.jar
COPY /deploy/database-config.edn .

EXPOSE 8080

CMD java -jar backend.jar
