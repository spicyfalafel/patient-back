SHELL = bash

up:
	docker-compose up -d

stop:
	docker-compose stop

down:
	docker-compose down

build:
	lein with-profile depl uberjar
	cd patient-front && export BACKEND= && shadow-cljs release app

docker-build-push:
	docker build -t spicyfalafel/pat-backend .
	docker image push spicyfalafel/pat-backend
	docker build -t spicyfalafel/pat-frontend ./patient-front/
	docker image push spicyfalafel/pat-frontend

kube-delete:
	kubectl delete all --all

kube-create:
	kubectl create -f kuber-pat.yaml

change-context:
	kubectl config set-context --current --namespace=patients


front-setup:
	rm -rf ./patient-front/resources/public/js
	rm -rf ./patient-front/.shadow-cljs
	cd patient-front && export BACKEND=http://localhost:8080 && npm ci && npx shadow-cljs watch app

