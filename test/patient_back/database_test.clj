(ns patient-back.database-test
  (:refer-clojure :exclude [assert])
  (:require [patient-back.database :as db]
            [clojure.java.jdbc :as jdbc]
            [clojure.test :refer [deftest testing is use-fixtures]]
            [patient-back.test-data :as f]
            [matcho.core :refer [assert] :as m]))

(defn fixt-create-temp-table
  "Фикстура для создания таблицы для проверки вставки/чтения даты"
  [t]
  (jdbc/db-do-commands f/test-db ["create table testing (datecol date);"])
  (t)
  (jdbc/db-do-commands f/test-db ["drop table testing"]))

(defn fixt-delete-patients
  [t]
  (jdbc/delete! f/test-db :patient [])
  (t))

(use-fixtures :each
  fixt-delete-patients)

(use-fixtures :once
  fixt-create-temp-table
  (fn [t] (t) (jdbc/delete! f/test-db :patient [])))

(deftest test-date-insertion
 ; Проверка извлечения и вставки даты
  (testing "valid date"
    (let [exp-date {:datecol #time/ld "2022-03-03"}]
       (is exp-date (jdbc/insert! f/test-db :testing {:datecol (db/parse-date "2022-03-03")}))
       (testing "date query"
         (is exp-date (jdbc/query f/test-db ["select * from testing"])))))
  (testing "bad date insert"
    (is (thrown? java.time.format.DateTimeParseException
                 (jdbc/insert! f/test-db :testing {:datecol (db/parse-date "2022-28-28")})))))



(def pat {:id 9999
          :firstname "aaa"
          :secondname "bbb"
          :sex "MALE"
          :birthdate #time/ld "1999-01-01"
          :address "Address"
          :polys 1234567812345678})

(deftest test-get-patients
  (jdbc/insert! f/test-db :patient pat)

  (def patient  (first(db/get-patients f/test-db)))

  (m/assert pat patient))

(deftest test-get-patient
  (jdbc/insert! f/test-db :patient pat)

  (m/assert pat (db/get-patient f/test-db (:id pat)))

  (m/assert nil? (db/get-patient f/test-db 8888)))


(deftest test-delete-patient
  (jdbc/insert! f/test-db :patient pat)
  (testing "with valid id"
    (def id (:id pat))
    (m/assert true? (db/delete-patient! f/test-db id))
    (m/assert nil? (db/get-patient f/test-db id)))
  (testing "with wrong id"
    (m/assert nil? (db/delete-patient! f/test-db 8888))))

(deftest test-replace-birthdate-str
  (def pat-raw (assoc pat :birthdate "1999-01-01"))
  (def replaced-pat (db/replace-birthdate-str pat-raw))
  (m/assert pat replaced-pat))

(deftest test-update-patient
  (jdbc/insert! f/test-db :patient pat)
  (def pat-upd {:id 9999
                :firstname "AAA"
                :secondname "BBB"
                :sex "FEMALE"
                :birthdate #time/ld "2000-02-02"})
  (m/assert true? (db/update-patient! f/test-db pat-upd))
  (m/assert pat-upd (db/get-patient f/test-db (:id pat-upd))))

(deftest test-insert-patient
  (m/assert pat (db/insert-patient! f/test-db pat))
  (m/assert pat (db/get-patient f/test-db (:id pat))))
