(ns patient-back.test-data)

(def test-db
  {:dbtype "postgresql"
   :dbname "testdb"
   :host "localhost"
   :user "postgres"
   :port 5432
   :password "root"})
