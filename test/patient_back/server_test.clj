(ns patient-back.server-test
  (:refer-clojure :exclude [assert])
  (:require [patient-back.database :as db]
            [clojure.java.jdbc :as jdbc]
            [clojure.test :refer [deftest testing is are use-fixtures]]
            [ring.mock.request :as mock]
            [cheshire.core :as json]
            [patient-back.server :as serv]
            [patient-back.validate :as v]
            [patient-back.test-data :as f]
            [clojure.pprint :as pp]
            [matcho.core :refer [assert] :as m]))


(defn fixt-delete-patients
  [t]
  (jdbc/delete! f/test-db :patient [])
  (t))

(use-fixtures :each
  fixt-delete-patients)

(use-fixtures :once
               (fn [t] (t) (jdbc/delete! f/test-db :patient [])))

(defn json-to-map [json]
  (json/parse-string json true))

(defn send-pat [method body endp]
  (let [resp (serv/handler f/test-db (-> (mock/request method (str "/api/patient/" endp))
                                         (mock/json-body body)))]
    (update-in resp [:body] json-to-map)))

(defn send-add-patient [body]
  (send-pat :post body ""))

(defn send-update-patient [id body]
  (send-pat :patch body id))

(defn send-delete-patient [id]
  (send-pat :delete {} id))

(defn send-get-patients []
  (send-pat :get {} ""))

(defn send-get-patient [id]
  (send-pat :get {} id))

(deftest test-crud
  (def resp-get (send-get-patients))
  (m/assert {:status 200 :body []} resp-get)

  (def pat {:firstname "Ivan"
            :secondname "Ivanov"
            :sex "MALE"})

  (def resp-add (send-add-patient pat))
  (m/assert {:status 200 :body {:id number?}} resp-add)

  (def id (:id (:body resp-add)))
  (def resp-get-one (send-get-patient id))
  (m/assert {:status 200 :body {:firstname "Ivan"
                                :secondname "Ivanov"
                                :sex "MALE"}}
            resp-get-one)

  (def resp-get (send-get-patients))

  (m/assert {:status 200 :body ^:matcho/strict
             [{:id number?
               :firstname "Ivan"
               :secondname "Ivanov"
               :sex "MALE"}]}
            resp-get))


(deftest test-get-patients
  (def resp-get (send-get-patients))
  (def patients (:body resp-get))
  (m/assert {:status 200 } resp-get)

  (for [p patients]
    (do
      (m/assert {:id number?
                 :firstname string?
                 :secondname string?
                 :sex string?
                 :birthdate string?
                 :address string?
                 :polys string?} p)
      (m/assert {:firstname #(and (v/len-in? % 3 64)
                                  (v/only-eng? %))
                 :secondname #(and (v/len-in? % 3 64)
                                   (v/only-eng? %))
                 :sex #(some #{%} ["MALE" "FEMALE"])
                 :birthdate v/val-date?
                 :polys #(<= 1000000000000000 % 9999999999999999)} p))))

(deftest test-get-patient
  (def pat {:id         9999
            :firstname  "aaa"
            :secondname "bbb"
            :sex        "MALE"
            :birthdate  #time/ld "1999-01-01"
            :address    "abc"
            :polys      1234123412341234})
  (def pat-date-raw (assoc pat :birthdate "1999-01-01"))
  (jdbc/insert! f/test-db :patient pat)

  (def get-resp (send-get-patient (:id pat)))
  (m/assert {:status 200 :body pat-date-raw} get-resp)

  (testing "check with bad id"
    (def bad-resp (send-get-patient -1))
    (m/assert {:status 404} bad-resp)))

(deftest test-add-patient
  (def pat-valid {:firstname  "aaa"
                  :secondname "bbb"
                  :sex        "MALE"
                  :birthdate  "1999-01-01"
                  :address    "abc"})
  (testing "valid"
    (def resp (send-add-patient pat-valid))
    (m/assert {:status 200 :body {:id number?}} resp))

  (testing "ignore id"
    (def resp (send-add-patient (assoc pat-valid :id -1)))
    (m/assert {:status 200 :body {:id #(and (number? %)
                                            (not (= -1 %)))}} resp))

  (testing "with invalid firstname"
    (def pat-inv-fname (assoc pat-valid :firstname "1digit"))
    (def resp (send-add-patient pat-inv-fname))
    (m/assert {:status 400 :body {:error "Wrong data"}} resp)

    (def pat-inv-fname (assoc pat-valid :firstname "a"))
    (def resp (send-add-patient pat-inv-fname))
    (m/assert {:status 400 :body {:error "Wrong data"}} resp))

  (testing "with invalid date"
    (def pat-inv-date (assoc pat-valid :birthdate "2000-28-28"))
    (def resp-bad (send-add-patient pat-inv-date))
    (m/assert {:status 400 :body {:error "Wrong data"}} resp-bad))

  (testing "with polys >16 digits"
    (def pat-inv-date (assoc pat-valid :polys 123456789123456789))
    (def resp-bad (send-add-patient pat-inv-date))
    (m/assert {:status 400 :body {:error "Wrong data"}} resp-bad))

  (testing "without address, polys, birthdate (only not-null sql)"
    (def pat (-> pat-valid
                (dissoc :address)
                (dissoc :birthdate)
                (dissoc :polys)))
    (def resp (send-add-patient pat))
    (m/assert {:status 200 :body {:id number?
                                  :firstname (:firstname pat-valid)
                                  :secondname (:secondname pat-valid)
                                  :sex (:sex pat-valid)
                                  :address nil
                                  :birthdate nil
                                  :polys nil}} resp)))

(deftest test-update-patient
  (def pat-valid {:firstname  "aaa"
                  :secondname "bbb"
                  :sex        "MALE"
                  :birthdate  "1999-01-01"
                  :address    "abc"})
  (def resp (send-add-patient pat-valid))
  (m/assert {:status 200 :body {:id number?}} resp)
  (def id (-> resp :body :id))

  (def pat-upd {:firstname "AAA"
                :secondname "BBB"
                :sex "FEMALE"
                :birthdate "1992-02-02"
                :address "ABC"})
  (def exp-pat (merge {:id id} pat-upd))

  (def resp-upd (send-update-patient id pat-upd))
  (m/assert {:status 200 :body exp-pat} resp-upd)

  (def resp-get (send-get-patient id))
  (m/assert {:status 200 :body exp-pat} resp-get)

  (testing "with invalid firstname"
    (def pat-inv-fname (assoc pat-valid :firstname "1!"))
    (def resp (send-update-patient id pat-inv-fname))
    (m/assert {:status 400 :body {:error "Wrong data"}} resp)

    (def resp-get (send-get-patient id))
    (m/assert {:status 200 :body exp-pat} resp-get))

  (testing "with no such id"
    (def pat-inv-id (assoc pat-valid :id 1234))
    (def resp (send-update-patient 1234 pat-inv-id))
    (m/assert {:status 400 :body {:error "Wrong data"}} resp)

    (def resp-get (send-get-patient id))
    (m/assert {:status 200 :body exp-pat} resp-get))

  (testing "body contains id which is different from url-id"
    (def pat-wr-id (->  pat-valid (assoc :id -100)
                       (assoc :firstname "abc")))

    (def resp (send-update-patient id pat-wr-id))
    (m/assert {:status 200 :body (assoc pat-wr-id :id id)} resp)

    (def resp-get (send-get-patient id))
    (m/assert {:status 200 :body (assoc pat-wr-id :id id)} resp-get)))


(deftest test-delete-patient
  (def pat-valid {:firstname  "aaa"
                  :secondname "bbb"
                  :sex        "MALE"
                  :birthdate  "1999-01-01"
                  :address    "abc"})
  (def resp (send-add-patient pat-valid))
  (m/assert {:status 200 :body {:id number?}} resp)
  (def id (-> resp :body :id))

  (def resp (send-delete-patient id))
  (m/assert {:status 200 :body {}} resp)

  (testing "wrong id"
    (def resp (send-delete-patient 6666))
    (m/assert {:status 404 :body {:error "No such id"}} resp)))

