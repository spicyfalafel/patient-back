(defproject patient-back "0.1.0-SNAPSHOT"
    :main patient-back.server
    :aot [patient-back.server]
    :dependencies [
                   [ring/ring-core            "1.9.5"]
                   [compojure                 "1.6.2"]
                   [org.clojure/clojure       "1.10.3"]
                   [ring/ring-json "0.5.1"]
                   [cheshire "5.10.2"]
                   [ring-cors "0.1.13"]
                   [org.clojure/java.jdbc "0.7.9"]
                   [org.postgresql/postgresql "42.3.3"]
                   [com.github.seancorfield/honeysql "2.2.868"]
                   [clojure.java-time "0.3.3"]
                   [environ "1.2.0"]
                   [http-kit "2.5.3"]
                   [healthsamurai/matcho "0.3.9"]]
    :plugins [[lein-environ "1.2.0"]]
    :profiles {:depl {:env {:database-config "deploy/database-config.edn"}}
               :dev {:env {:database-config "database-config.edn"}
                     :dependencies [[ring/ring-mock "0.4.0"]]}
               :test {:env {:database-config "database-test-config.edn"}
                      :dependencies [[ring/ring-mock "0.4.0"]]}})
